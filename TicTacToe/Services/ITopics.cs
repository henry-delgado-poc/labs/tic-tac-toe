﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TicTacToe.Models;

namespace TicTacToe.Services
{
    public interface ITopics
    {
        Task<bool> AddTopicAsync(Topic topic);
        Task<bool> UpdateTopicAsync(Topic topic);
        Task<bool> DeleteTopicAsync(long id);
        Task<Topic> GetTopicAsync(long id);
        Task<IEnumerable<Topic>> GetTopicsAsync(int maxResult = 9);
    }
}
