﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TicTacToe.Models;
namespace TicTacToe.Services
{
    public interface IAnswers
    {
        Task<bool> AddAnswerAsync(Answer answer);
        Task<bool> UpdateAnswerAsync(Answer answer);
        Task<bool> DeleteAnswerAsync(long id);
        Task<Answer> GetAnswerAsync(long id);
        Task<List<Answer>> GetAnswersPerQuestionIdAsync(long questionId);
    }
}
