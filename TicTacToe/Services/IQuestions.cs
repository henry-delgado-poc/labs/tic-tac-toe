﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TicTacToe.Models;
namespace TicTacToe.Services
{
    public interface IQuestions
    {
        Task<bool> AddQuestionAsync(Question question);
        Task<bool> UpdateQuestionAsync(Question question);
        Task<bool> DeleteQuestionAsync(long id);
        Task<Question> GetQuestionAsync(long id);
        Task<Question> GetQuestionPerTopicIdAsync(long topicId, List<long> exludeQuestionIds = null);
    }
}
