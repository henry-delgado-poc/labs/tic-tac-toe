﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicTacToe.Helpers;
using TicTacToe.Models;

namespace TicTacToe.Services
{
    public class MockTopicStore: ITopics
    {
        private List<Topic> _topics;
        public MockTopicStore()
        {
            _topics = new List<Topic>()
            {
                new Topic { Id = 1, Name = "Quien Soy"},
                new Topic { Id = 2, Name = "Varones"},
                new Topic { Id = 3, Name = "Geografia"},
                new Topic { Id = 4, Name = "Animales"},
                new Topic { Id = 5, Name = "Oficios"},
                new Topic { Id = 6, Name = "Mujeres"},
                new Topic { Id = 7, Name = "Reyes"},
                new Topic { Id = 8, Name = "Libro"},
                new Topic { Id = 9, Name = "Cierto o Falso"},
            };
        }
                
        public async Task<bool> AddTopicAsync(Topic topic)
        {
            _topics.Add(topic);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateTopicAsync(Topic topic)
        {
            var oldItem = _topics.Where((Topic arg) => arg.Id == topic.Id).FirstOrDefault();
            _topics.Remove(oldItem);
            _topics.Add(topic);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteTopicAsync(long id)
        {
            var oldItem = _topics.Where((Topic arg) => arg.Id == id).FirstOrDefault();
            _topics.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<Topic> GetTopicAsync(long id)
        {
            return await Task.FromResult(_topics.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<Topic>> GetTopicsAsync(int maxResult = 9)
        {
            _topics.Shuffle();
            return await Task.FromResult(_topics.Take(maxResult));
        }
    }
}
