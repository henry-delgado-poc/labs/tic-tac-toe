﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicTacToe.Helpers;
using TicTacToe.Models;

namespace TicTacToe.Services
{
    public class MockQuestionStore: IQuestions
    {
        private List<Question> _questions = new List<Question>();
        private const string BASE_QUESTION = "simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s";
        public MockQuestionStore()
        {
            int questionId = 1;
            for (int topicId = 1; topicId <= 9; topicId++)
            {
                for (int q = 1; q <= 100; q++)
                {
                    _questions.Add(new Question { TopicId = topicId, Id = questionId, Name = questionId + "| " + BASE_QUESTION });
                    questionId++;
                }
            }
        }

        public async Task<bool> AddQuestionAsync(Question question)
        {
            _questions.Add(question);
            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteQuestionAsync(long id)
        {
            var oldItem = _questions.Where((Question arg) => arg.Id == id).FirstOrDefault();
            _questions.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<Question> GetQuestionPerTopicIdAsync(long topicId, List<long> excludeQuestionIds = null)
        {
            if (_questions == null || !_questions.Any() || topicId <1)
                return null;

            var result = excludeQuestionIds != null
                    ? _questions.Where(x => x.TopicId == topicId && !excludeQuestionIds.Contains(x.Id)).ToList()
                    : _questions.Where(x => x.TopicId == topicId).ToList();

            if (!result.Any())
                return null;

            result.Shuffle();
            return await Task.FromResult(result.First());            
        }

        public async Task<Question> GetQuestionAsync(long id)
        {
            return await Task.FromResult(_questions.FirstOrDefault(s => s.Id == id));
        }
              
        public async Task<bool> UpdateQuestionAsync(Question question)
        {
            var oldItem = _questions.Where((Question arg) => arg.Id == question.Id).FirstOrDefault();
            _questions.Remove(oldItem);
            _questions.Add(question);

            return await Task.FromResult(true);
        }
    }
}
