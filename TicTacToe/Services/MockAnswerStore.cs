﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicTacToe.Helpers;
using TicTacToe.Models;

namespace TicTacToe.Services
{
    public class MockAnswerStore: IAnswers
    {
        private List<Answer> _answers = new List<Answer>();
        public MockAnswerStore()
        {
            int answerId = 1;
            for (int questionId = 1; questionId <= 900; questionId++)
            {
                for (int a = 1; a <= 3; a++)
                {
                    _answers.Add(new Answer { QuestionId = questionId, Id = answerId, Text = "Answer " + a, IsCorrect = a == 2 });
                    answerId++;
                }
            }            
        }

        public async Task<bool> AddAnswerAsync(Answer answer)
        {
            _answers.Add(answer);
            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteAnswerAsync(long id)
        {
            var oldItem = _answers.Where((Answer arg) => arg.Id == id).FirstOrDefault();

            _answers.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<Answer> GetAnswerAsync(long id)
        {
            return await Task.FromResult(_answers.FirstOrDefault(s => s.Id == id));
        }

        public async Task<List<Answer>> GetAnswersPerQuestionIdAsync(long questionId)
        {
            var list = _answers.Where(s => s.QuestionId == questionId).ToList();
            
            if (!list.Any())
                return null;
            list.Shuffle();
            return await Task.FromResult(list);
        }

        public async Task<bool> UpdateAnswerAsync(Answer answer)
        {
            var oldItem = _answers.Where((Answer arg) => arg.Id == answer.Id).FirstOrDefault();
            _answers.Remove(oldItem);
            _answers.Add(answer);

            return await Task.FromResult(true);
        }
    }
}
