﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CustomXamarinObjects;
using Rg.Plugins.Popup.Extensions;
using TicTacToe.Helpers;
using TicTacToe.Models;
using TicTacToe.Services;
using TicTacToe.Views;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace TicTacToe.ViewModels
{
    public class GameViewModel: BaseViewModel
    {
        private bool isPlayerX = true;
        private int playerXCounter = 0;
        private int player0Counter = 0;
        private int totalCounts = 0;
        private bool isGameOver = false;
        private string playerDisplay = "Player: X";
        private string[] gameArray = new string[9];
        private Question question;
        private List<Answer> answers = new List<Answer>();
        private Grid _gameGrid;
        private Page _page;
        private bool isAnimating;
        private string playAgainButtonText = "Play Again!";
        private bool pendingAnswer = false;
        private List<long> questionsAsked = new List<long>();

        public ObservableCollection<Topic> Topics { get; } = new ObservableCollection<Topic>();

        public Command StartGameCommand { get; }
        public Command PlayerClickedButtonCommand { get; }
        public Command LoadTopicsCommand { get; }
        public Command LoadQuestionsCommand { get; }
        public Command LoadAnswersCommand { get; }
        public Command<long> AnswerSelectedCommand { get; }
        
        public ITopics TopicStore => DependencyService.Get<ITopics>();
        public IQuestions QuestionStore => DependencyService.Get<IQuestions>();
        public IAnswers AnswerStore => DependencyService.Get<IAnswers>();

        public GameViewModel(Grid gameGrid, Page page)
        {

            _gameGrid = gameGrid;
            _page = page;
            LoadQuestionsCommand = new Command<long>(async (long topicId) => await ExecuteLoadQuestionsCommand(topicId));
            LoadAnswersCommand = new Command<long>(async (long id) => await ExecuteLoadAnswersCommand(id));
            AnswerSelectedCommand = new Command<long>(async (long id) => await ExecuteAnswerSelectedCommand(id));
            PlayerClickedButtonCommand = new Command<TicTacToeButton>(async (x) => await PlayerPlayed(x));
            StartGameCommand = new Command(() => StartGame());
            StartGame();                        
        }

        public GameViewModel()
        {
            IsBusy = true;
        }

        async Task ExecuteLoadTopicsCommand()
        {
            IsBusy = true;

            try
            {
                Topics.Clear();
                var topics = await TopicStore.GetTopicsAsync();

                foreach (var item in topics)
                {
                    Topics.Add(item);
                }                

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }               

        async Task ExecuteLoadQuestionsCommand(long topicId)
        {
            IsBusy=true;
            try
            {
                
                Question = await QuestionStore.GetQuestionPerTopicIdAsync(topicId, questionsAsked);
                questionsAsked.Add(Question.Id);               

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }

        }

        async Task ExecuteLoadAnswersCommand(long questionId)
        {
            IsBusy = true;
            try
            {
                Answers = await AnswerStore.GetAnswersPerQuestionIdAsync(questionId);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }

        }
                
        async Task ExecuteAnswerSelectedCommand(long id)
        {
            await Task.Yield();

            IsBusy = true;
            try
            {
                var selectedAnswer = Answers.First(x => x.Id == id);
                if(selectedAnswer.IsCorrect)
                {
                    //correct answer!!

                    //Print player name on tile
                }
                else
                {
                    //bad answer !!
                }
                //todo
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public async Task PlayerPlayed(TicTacToeButton btn)
        {
            if (btn == null || !btn.IsAvailable)
                return;

            if(!IsPendingAnswer)
            {
                await ExecuteLoadQuestionsCommand(btn.TopicId);
                await ExecuteLoadAnswersCommand(Question.Id);
                IsPendingAnswer = true;
            }
            var topic = Topics.First(x => x.Id == Question.TopicId).Name;
            var result = await QuestionsPopup.ConfirmConferenceAttendance(_page.Navigation, Question.Name, Answers, topic, isPlayerX ? "Player: X" : "Player: 0");

            string playerAnswer = string.Empty;
            while(IsPendingAnswer)
            {
                playerAnswer = await _page.DisplayActionSheet(Question.Name, "Cancel", null, Answers.Select(x => x.Text).ToArray<string>());
                IsPendingAnswer = playerAnswer.ToUpper() == "CANCEL";
            }
            

            if (Answers.First(x=> x.IsCorrect).Text == playerAnswer)
            {
                //correct
                //once question answered
                if (isPlayerX)
                {
                    btn.Text = btn.Player = "X";
                    btn.BackgroundColor = (Color)Application.Current.Resources["SelectedX"];
                    PlayXCounter++;
                    gameArray[btn.BoardIndex] = "X";
                }
                else
                {
                    btn.Text = btn.Player = "0";
                    btn.BackgroundColor = (Color)Application.Current.Resources["Selected0"];
                    Play0Counter++;
                    gameArray[btn.BoardIndex] = "0";
                }
                btn.FontSize = (OnIdiom<double>)Application.Current.Resources["TicTacToeSelectedButtonFontSize"];
                btn.FontAttributes = FontAttributes.Bold;
                btn.IsAvailable = false;

                checkWinner();
            }
            else
            {
                //wrong. Animation or something
            }

            TotalCounter++;
            isPlayerX = !isPlayerX;
            PlayerDisplay = isPlayerX ? "Player: X" : "Player: 0";



        }

        public bool IsPendingAnswer
        {
            get => pendingAnswer;
            set
            {
                if (value == pendingAnswer)
                    return;
                pendingAnswer = value;
                OnPropertyChanged();
            }
        }

        public bool IsAnimating
        {
            get => isAnimating;
            set
            {
                if (value == isAnimating)
                    return;
                isAnimating = value;
                OnPropertyChanged();
            }
        }

        public int TotalCounter
        {
            get => totalCounts;
            set
            {
                if (value == totalCounts)
                    return;
                totalCounts = value;
                OnPropertyChanged();
            }
        }

        public Question Question
        {
            get => question;
            set
            {
                if (value == question)
                    return;
                question = value;
                OnPropertyChanged();
            }
        }

        public List<Answer> Answers
        {
            get => answers;
            set
            {
                if (value == answers)
                    return;
                answers.Clear();
                answers = value;
                OnPropertyChanged();
            }
        }


        public int PlayXCounter
        {
            get => playerXCounter;
            set
            {
                if (value == playerXCounter)
                    return;
                playerXCounter = value;
                OnPropertyChanged();
            }
        }

        public int Play0Counter
        {
            get => player0Counter;
            set
            {
                if (value == player0Counter)
                    return;
                player0Counter = value;
                OnPropertyChanged();
            }
        }

        public string PlayerDisplay
        {
            get => playerDisplay;
            set
            {
                if (value == playerDisplay)
                    return;
                playerDisplay = value;
                OnPropertyChanged();
            }
        }

        public bool IsGameOver
        {
            get => isGameOver;
            set
            {
                if (value == isGameOver)
                    return;
                isGameOver = value;
                if(isGameOver)
                {
                    PlayAgainButtonText = "Play Again!";
                }
                else
                {
                    PlayAgainButtonText = "Start Over!";
                }
                OnPropertyChanged();
            }
        }

        public string PlayAgainButtonText
        {
            get => playAgainButtonText;
            set
            {
                if (value == playAgainButtonText)
                    return;
                playAgainButtonText = value;
                OnPropertyChanged();
            }
            
        }

        private void StartGame()
        {
            ExecuteLoadTopicsCommand().Wait();
            isPlayerX = true;
            PlayXCounter = Play0Counter = 0;
            IsGameOver = false;
            ResetButtons(_gameGrid);
            PlayerDisplay = "Player: X";            
        }
               

        private void ResetButtons(Grid gameGrid)
        {
            int idx = 0;
            double buttonFontSize = (OnIdiom<double>)Application.Current.Resources["TicTacToeButtonFontSize"];
            gameGrid.Children.ForEach((child) => {
                if (child is TicTacToeButton)
                {
                    var btn = ((TicTacToeButton)child);
                    btn.Reset();
                    btn.Text = Topics[idx].Name;
                    btn.TopicId = Topics[idx].Id;
                    btn.FontSize = buttonFontSize;
                    idx++;
                }

            });
            for (int i = 0; i < 9; i++)
            {
                gameArray[i] = "";
            }
        }

        private void checkWinner()
        {
            if (CheckMoves("X"))
            {
                PlayerDisplay = "Player X Wins !!!";

                IsGameOver = true;
            }
            else if (CheckMoves("0"))
            {
                PlayerDisplay = "Player 0 Wins !!!";
                IsGameOver = true;
            }
            else
            {
                if (Array.IndexOf(gameArray, null) == -1 && Array.IndexOf(gameArray, "") == -1)
                {
                    //tie
                    PlayerDisplay = "Tie!";
                    IsGameOver = true;

                }
            }
        }

        private bool CheckMoves(string player)
        {
            //horizontal moves
            for (int i = 0; i < 9; i = i + 3)
            {
                if (gameArray[i] == player && gameArray[i + 1] == player && gameArray[i + 2] == player)
                {
                    return true;
                }
            }
            //vertical
            for (int i = 0; i <= 2; i++)
            {
                if (gameArray[i] == player && gameArray[i + 3] == player && gameArray[i + 6] == player)
                {
                    return true;
                }
            }
            //diagonal
            if (gameArray[0] == player && gameArray[4] == player && gameArray[8] == player)
            {
                return true;
            }
            if (gameArray[2] == player && gameArray[4] == player && gameArray[6] == player)
            {
                return true;
            }
            return false;
        }

        
    }
}
