﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicTacToe.Models;
using Xamarin.Forms;

namespace TicTacToe.ViewModels
{
    public class QuestionViewModel: BaseViewModel
    {
        private string question;
        private List<Answer> answers = new List<Answer>();
        private string playerName;
        private string topic;

        public Command PlayerClickedButtonCommand { get; }

        public QuestionViewModel(string _question, List<Answer> _answers, string _topic, string _player)
        {
            Question = _question;
            Answers = _answers;
            Topic = _topic;
            PlayerName = $"({_player})";
        }

        public QuestionViewModel()
        {
            PlayerClickedButtonCommand = new Command<int>((answerId) => PlayerPlayed(answerId));
        }

        private Task PlayerPlayed(int answerId)
        {
            var playerAnswer = Answers.First(x => x.Id == answerId);
            if(playerAnswer.IsCorrect)
            {
                //TODO
            }
            throw new NotImplementedException();
        }

        public string Question
        {
            get => question;
            set
            {
                if (value == question)
                    return;
                question = value;
                OnPropertyChanged();
            }
        }

        public List<Answer> Answers
        {
            get => answers;
            set
            {
                if (value == answers)
                    return;
                answers.Clear();
                answers = value;
                OnPropertyChanged();
            }
        }

        public string Topic
        {
            get => topic;
            set
            {
                if (topic == value)
                    return;
                topic = value;
                OnPropertyChanged();
            }
        }

        public string PlayerName
        {
            get => playerName;
            set
            {
                if (playerName == value)
                    return;
                playerName = value;
                OnPropertyChanged();
            }
        }
    }
}
