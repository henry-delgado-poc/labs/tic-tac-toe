﻿using Xamarin.Forms;

namespace TicTacToe.ViewModels
{
    //[QueryProperty(nameof(An), nameof(ItemId))]
    public class GameOverViewModel: BaseViewModel
    {
        private string player;
        private string topic;
        private string question;
        private string correctAnswer;
        private string message;
        ///public long AnswerId { get; set; }

        public GameOverViewModel()
        {
            CancelCommand = new Command(OnCancel);
        }

        public Command CancelCommand { get; }

        private async void OnCancel()
        {
            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }

        public string Player
        {
            get => player;
            set => SetProperty(ref player, value);
        }

        public string Topic
        {
            get => topic;
            set => SetProperty(ref topic, value);
        }

        public string Question
        {
            get => question;
            set => SetProperty(ref question, value);
        }

        public string CorrectAnswer
        {
            get => correctAnswer;
            set => SetProperty(ref correctAnswer, value);
        }

        public string Message
        {
            get => message;
            set => SetProperty(ref message, value);
        }
    }
}
