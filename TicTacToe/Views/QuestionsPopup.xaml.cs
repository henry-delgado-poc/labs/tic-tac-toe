﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using TicTacToe.Models;
using TicTacToe.ViewModels;
using Xamarin.Forms;

namespace TicTacToe.Views
{
    public partial class QuestionsPopup : PopupPage, INotifyPropertyChanged
    {
        private readonly Action<bool> setResultAction;
        
        public void CancelAttendanceClicked(object sender, EventArgs e)
        {
            IsBusy = false;
            setResultAction?.Invoke(false);
            this.Navigation.PopPopupAsync().ConfigureAwait(false);
        }

        public void ConfirmAttendanceClicked(object sender, EventArgs e)
        {
            IsBusy = false;
            setResultAction?.Invoke(true);
            this.Navigation.PopPopupAsync().ConfigureAwait(false);
        }
               
        public QuestionsPopup(Action<bool> setResultAction, string question, List<Answer> answers, string topic, string player)
        {
            InitializeComponent();
            BindingContext = new QuestionViewModel(question, answers, topic, player);
            IsBusy = true;
            this.setResultAction = setResultAction;
            
        }

        public static async Task<bool> ConfirmConferenceAttendance(INavigation navigation, string question, List<Answer> answers, string topic, string player)
        {
            TaskCompletionSource<bool> completionSource = new TaskCompletionSource<bool>();

            void callback(bool didConfirm)
            {
                completionSource.TrySetResult(didConfirm);
            }

            var popup = new QuestionsPopup(callback, question, answers, topic, player);

            await navigation.PushPopupAsync(popup);

            return await completionSource.Task;
        }
    }
}
