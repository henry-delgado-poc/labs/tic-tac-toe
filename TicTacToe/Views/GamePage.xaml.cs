﻿using TicTacToe.ViewModels;
using Xamarin.Forms;
namespace TicTacToe.Views
{
    public partial class GamePage : ContentPage
    {
        public GamePage()
        {
            InitializeComponent();
            BindingContext = new GameViewModel(gameGrid, this);
            
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();            
        }
    }
}
