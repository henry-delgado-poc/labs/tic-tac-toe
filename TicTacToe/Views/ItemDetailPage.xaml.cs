﻿using System.ComponentModel;
using Xamarin.Forms;
using TicTacToe.ViewModels;

namespace TicTacToe.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}