﻿namespace TicTacToe.Models
{
    public class Question
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long TopicId { get; set; }
    }
}
